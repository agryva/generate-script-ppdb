import pymongo
import xlrd
import datetime
import json
import os
import math
from bson.objectid import ObjectId

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
myColRegis = mydb["ppdb_registrations_2020"]

pendaftar = myColRegis.find({"tahap_pendaftaran": 2, "cara_pendaftaran": "sekolah_tujuan", "level_pendaftaran": "nhun", "total_rapor": 0})

print("Sekolah Tujuan")
i=0
for regis in pendaftar:
    myColRegis.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "status" : "reject",
            "alasan": "Nilai Rapor Harap Diisi Terlebih Dahulu"
        }
    })
    i+=1
    print(i)


pendaftar = myColRegis.find({"tahap_pendaftaran": 2, "cara_pendaftaran": "sekolah_asal", "level_pendaftaran": "nhun", "total_rapor": 0})
print("Sekolah Asal")
i=0
for regis in pendaftar:
    myColRegis.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "status" : "reject",
            "alasan": "Nilai Rapor Harap Diisi Terlebih Dahulu"
        }
    })
    i+=1
    print(i)

pendaftar = myColRegis.find({"tahap_pendaftaran": 2, "cara_pendaftaran": "mandiri", "level_pendaftaran": "nhun", "total_rapor": 0})
print("Mandiri")
i=0
for regis in pendaftar:
    myColRegis.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "status" : "reject",
            "alasan": "Nilai Rapor Harap Diisi Terlebih Dahulu"
        }
    })
    i+=1
    print(i)