import pymongo
import xlrd
import datetime
import json
import os
import math
from bson.objectid import ObjectId

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycoloptions = mydb["ppdb_options"]
mycolschool = mydb["ppdb_schools"]

schools = mycolschool.find({
    "level": "senior"
})

for school in schools:
    options = mycoloptions.find({
        "school_id": school["_id"],
        "type": "tenaga-kesehatan"
    })

    for option in options:
        mycoloptions.update_one({
            "_id": option["_id"]
        }, {
            "$set": {
                "name": str(option["name"]).replace("TENAGA KESEHATAN", " - TENAGA KESEHATAN")
            }
        })
        print(option["name"])

# print("SMA")
# schoolSMA = mycolschool.find({
#     "level": "senior"
# })

# for sma in schoolSMA:
#     options = mycoloptions.find({
#         "school_id": sma["_id"]
#     })

#     for option in options:
#         if option['type'] == "ketm" :
#             current_st = 0
            
#             print(option['name'])
#             try :
#                 if option['current_student'] is not None:
#                     current_st = option['current_student']
#             except KeyError:
#                 pass
#             total_quota = option['total_quota']
#             tenaga_kesehatan = math.floor(total_quota * 0.02)
#             if tenaga_kesehatan < 1:
#                 tenaga_kesehatan = 1

#             name = str(option['name']).split("-")
#             mycoloptions.insert_one({
#                 "verified_cadisdik" : True,
#                 "no_color_blind" : option['no_color_blind'],
#                 "name" : name[0] + "TENAGA KESEHATAN" ,
#                 "type" : "tenaga-kesehatan",
#                 "rombel" : option['rombel'],
#                 "quota" : tenaga_kesehatan,
#                 "total_quota" : option['total_quota'],
#                 "method" : "tenaga-kesehatan",
#                 "school_id" : option['school_id'],
#                 "major_id" : option['major_id'],
#                 "quota_per_class" : option['quota_per_class'],
#                 "quota_new" : 0,
#                 "total_quota_new" : 0,
#                 "current_student_new" : 0,
#                 "quota_per_class_new" : 0,
#                 "__v" : 0,
#                 "rombel_new" : 0,
#                 "current_student" : current_st
#             })
#             mycoloptions.update_one({
#                 "_id": option['_id']
#             }, {
#                 "$set": {
#                     "quota": (option["quota"] - tenaga_kesehatan)
#                 }
#             })



# print("SMK")
# schoolSMK = mycolschool.find({
#     "level": "vocational"
# })

# for smk in schoolSMK:
#     options = mycoloptions.find({
#         "school_id": smk["_id"]
#     })

#     for option in options:
#         if option['type'] == "ketm" :
#             current_st = 0
            
#             print(option['name'])
#             try :
#                 if option['current_student'] is not None:
#                     current_st = option['current_student']
#             except KeyError:
#                 pass
#             total_quota = (option['rombel'] * option['quota_per_class']) - current_st
#             tenaga_kesehatan = math.floor(total_quota * 0.02)
#             if tenaga_kesehatan < 1:
#                 tenaga_kesehatan = 1
#             name = str(option['name']).split("-")
#             mycoloptions.insert_one({
#                 "verified_cadisdik" : True,
#                 "no_color_blind" : option['no_color_blind'],
#                 "name" : name[0] + "-" + name[1] + " TENAGA KESEHATAN" ,
#                 "type" : "tenaga-kesehatan",
#                 "rombel" : option['rombel'],
#                 "quota" : tenaga_kesehatan,
#                 "total_quota" : option['total_quota'],
#                 "method" : "tenaga-kesehatan",
#                 "school_id" : option['school_id'],
#                 "major_id" : option['major_id'],
#                 "quota_per_class" : option['quota_per_class'],
#                 "quota_new" : 0,
#                 "total_quota_new" : 0,
#                 "current_student_new" : 0,
#                 "quota_per_class_new" : 0,
#                 "__v" : 0,
#                 "rombel_new" : 0,
#                 "current_student" : current_st
#             })
#             mycoloptions.update_one({
#                 "_id": option['_id']
#             }, {
#                 "$set": {
#                     "quota": (option["quota"] - tenaga_kesehatan)
#                 }
#             })
