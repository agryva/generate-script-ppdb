import pymongo
import json
import os

# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolSchools = mydb["ppdb_schools"]
mycolUser = mydb["ppdb_users"]

arraySchool = mycolSchools.find({"level": "slb"})

i=0
for school in arraySchool:
    mycolUser.update_one({
        "school": str(school["_id"]),
        "role": "juniorschool-admin"
    }, {
        "$set": {
            "role": "highschool-admin"
        }
    })
    i+=1
    print(i)