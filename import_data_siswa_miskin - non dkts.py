import pymongo
import xlrd
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
mydb = myclient["ppdb"]
mycolNONDKTS = mydb["ppdb_non_dtks"]


def checkValidationData(data):
    # if data == "[NULL]" or data == "NULL":
    #     return "-"
    # elif data == "PEREMPUAN":
    #     return "F"
    # elif data == "LAKI-LAKI":
    #     return "M"
    # elif data == "42":
    #     return ""
    # else:
    return data

dir = './NONDKTS/'
allfiles = os.listdir(dir)
files = [ fname for fname in allfiles if fname.endswith('.xlsx')]

for fileName in files:
    dirname = os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__)), dir), fileName)      
    wb = xlrd.open_workbook(dirname) 
    sh = wb.sheet_by_index(0)
    dataMiskin=[]
    city=""
    index=0
    for i in range(sh.nrows):
        if i != 0:
            value = sh.row_values(i)
            city=str(value[1]).replace("KABUPATEN", "KAB.")
            no_kk=str(value[9].encode('ascii', 'ignore').decode('unicode_escape'))
            # no_kk=no_kk.replace('\uxa0', '')
            no_kk=no_kk.strip()
            no_kk=no_kk.replace("`", "").replace(".", "")
            print(city, str(value[8]).strip())
            dataMiskin.append({
                "city":str(value[1]).replace("KABUPATEN", "KAB."),
                "district": checkValidationData(str(value[2])),
                "subdistrict": checkValidationData(str(value[3])),
                "rt": (str(value[4])),
                "rw": (str(value[5])),
                "address": (str(value[6])),
                "name": value[7].strip(),
                "nik": checkValidationData(str(value[8]).strip()),
                "no_kk": no_kk,
            })
            i+=1
            print(i)
    # with open('data_miskin_non_dkts'+city+".json", 'w', encoding='utf-8') as outfile:
    #     json.dump(dataMiskin, outfile, ensure_ascii=False, indent=4)
    mycolNONDKTS.insert_many(dataMiskin)
    print(city+" Done")

# for fileName in files:
#     dirname = os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__)), dir), fileName)
#     print(dirname)