from dbfread import DBF
import pymongo
import xlrd
import json
import os

dataRapor=[]
myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
mydb = myclient["ppdbCimahi"]
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
#mydb = myclient["ppdb_cimahi"]
myJunior = mydb["ppdb_unjuniors"]

dirname = os.path.join(os.path.dirname(os.path.abspath(__file__)), "REVISIRAPOR266.xlsx")
print(dirname)
wb = xlrd.open_workbook(dirname) 
sh = wb.sheet_by_index(0)

def checkValidationScore(score):
    score = str(score).strip()
    if score == '':
        return 0
    else:
        return "{:.2f}".format(float(score))

dataRapor = []

for i in range(sh.nrows):
    if i >= 9:
        value = sh.row_values(i)
        un = str(value[2]).strip().replace('.','')
        un_year = str(value[1]).replace(".0", "")
        rapor=[]
        for j in range(5):
            rapor.append({
                "sms": (j + 7),
                "nilai": checkValidationScore(value[j + 8])
            })
        dataRapor.append({
            "un_year": un_year,
            "un": un,
            "rapor": rapor,
            "nik": str(value[5]),
            "coordinate_lat": str(value[6]),
            "coordinate_lng": str(value[7])
        })

index = 0
for rapor in (dataRapor):
    index+=1
    result = myJunior.update_one({
        "$and": [
            {"un_year": rapor['un_year']},
            {"un": rapor['un']},
            {"un_type": "reguler-sd"}
        ]
    }, {
        "$set": {
            "rapor": rapor["rapor"],
            "nik": rapor["nik"],
            "coordinate_lat": rapor["coordinate_lat"],
            "coordinate_lng": rapor["coordinate_lng"]
        }
    })
    if result.matched_count == 0:
        print(rapor['un'])


#mySchools.insert_many(dataRapor)

#schools = mySchools.find({"level": "tk"})

#for sc in schools:
#    myUser.insert_one({
#        "name": sc["name"],
#        "username": "TK"+sc["npsn"],
#        "password": "TK"+sc["npsn"],
#        "role": "tkschool-admin",
#        "school": str(sc["_id"])
#    })