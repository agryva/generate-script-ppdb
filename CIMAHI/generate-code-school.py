from dbfread import DBF
import pymongo
import xlrd
import json
import os

dataRapor=[]
myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
mydb = myclient["ppdbCimahi"]
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
# mydb = myclient["ppdb_cimahi"]
mySchool = mydb["ppdb_schools"]

schools = mySchool.find({"code": {"$exists": False}, "level": "elementary"})

code = 10127

def cekData(cd):
    sc = mySchool.find_one({"code": cd})
    if sc is None:
        return cd
    else:
        return cekData(cd + 1)

for school in schools:
    code=cekData(code)
    mySchool.update_one({"_id": school["_id"]}, {"$set": {"code": code}})
    print(school["_id"])
    code+=1
