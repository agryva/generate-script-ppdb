from dbfread import DBF
import pymongo
import xlrd
import json
import os

dataRapor=[]
myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
mydb = myclient["ppdbCimahi"]
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
#mydb = myclient["ppdb_cimahi"]
myJunior = mydb["ppdb_unjuniors"]
myRegistrant = mydb["ppdb_registrations_2020"]

registrants = myRegistrant.find({})

for regis in registrants:
    junior=myJunior.find_one({"_id": regis["id_junior"]})
    if junior is not None:
        print(regis["id_junior"])
        file_ktp = ''
        if "file_ktp" in junior:
            file_ktp=junior["file_ktp"]
        file_kelulusan = ''
        if "file_kelulusan" in junior:
            file_kelulusan=junior["file_kelulusan"]
        file_koordinat = ''
        if "file_koordinat" in junior:
            file_koordinat=junior["file_koordinat"]
        peryataan_mutlak = ''
        if "peryataan_mutlak" in junior:
            peryataan_mutlak=junior["peryataan_mutlak"]
        file_akte = ''
        if "file_akte" in junior:
            file_akte=junior["file_akte"]
        file_kk = ''
        if "file_kk" in junior:
            file_kk=junior["file_kk"]
        file_pernyataan_ortu = ''
        if "file_pernyataan_ortu" in junior:
            file_pernyataan_ortu=junior["file_pernyataan_ortu"]
        print(file_kk)
        myRegistrant.update_one({"_id":regis["_id"]}, {
            "$set": {
                "file_ktp": file_ktp,
                "file_kelulusan": file_kelulusan,
                "file_koordinat": file_koordinat,
                "peryataan_mutlak": peryataan_mutlak,
                "file_akte": file_akte,
                "file_kk": file_kk
            }
        })
