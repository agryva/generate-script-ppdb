import pymongo
import json
import os
import datetime
import math
from geopy.distance import great_circle

myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
mydb = myclient["ppdbCimahi"]
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
#mydb = myclient["ppdb_cimahi"]
#mydb = myclient["ppdb"]
mycolJunior = mydb["ppdb_unjuniors"]
mycolSchool = mydb["ppdb_schools"]
mycolRegistration = mydb["ppdb_registrations_2020"]

def calculate(lat1, lon1, lat2, lon2) :
    if (lat1 == lat2) and (lon1 == lon2) :
        return 0
    else :
        radlat1 = math.pi * lat1 / 180
        radlat2 = math.pi * lat2 / 180
        theta = lon1 - lon2
        radtheta = math.pi * theta / 180
        dist = math.sin(radlat1) * math.sin(radlat2) + math.cos(radlat1) * math.cos(radlat2) * math.cos(radtheta)
        if dist > 1:
            dist = 1
        dist=math.acos(dist)
        dist=dist*180/math.pi
        dist=dist*60*1.1515
        dist=dist* 1.609344 * 1000
        return float("{:.3f}".format(dist))

def calculatev2(lat1, lon1, lat2, lon2) :
    if (lat1 == lat2) and (lon1 == lon2) :
            return 0
    else :
       return float("{:.3f}".format(great_circle((lat1,lon1), (lat2, lon2)).meters))

regisArray = mycolRegistration.find({})

print("SEMUA")

for regis in regisArray:
    latDomisili = str(regis["coordinate_lat"]).strip().replace(",", ".").replace("°", ".").replace(" ", ".")
    lonDomisili = str(regis["coordinate_lng"]).strip().replace(",", ".").replace("°", ".").replace(" ", ".")

    if (latDomisili != "undefined") and (lonDomisili != "undefined"):
        print(regis["_id"])
        
        if "first_choice_school" in regis:
            school = mycolSchool.find_one({"_id": regis["first_choice_school"]})
            if school is not None:
                latSchool=school["coordinate_lat"]
                lonSchool=school["coordinate_lng"]
                if (latSchool != "" and latSchool != "0.0") and (lonSchool != "" and lonSchool != "0.0") and (latDomisili != "" and latDomisili != "0.0") and (lonDomisili != "" and lonDomisili != "0.0"):
                    mycolRegistration.update_one({
                        "_id": regis["_id"]
                    }, {
                        "$set": {
                            "score_jarak_1": calculate(float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
                        }
                    })

# regisArray = mycolRegistration.find({"level_pendaftaran": "ketm", "document.ketm.domisili": {"$exists": True}})

# print("KETM")

# for regis in regisArray:
#     latDomisili = regis["document"]["ketm"]["domisili"]["coordinate_lat"]
#     lonDomisili = regis["document"]["ketm"]["domisili"]["coordinate_long"]
#     print(regis["code"])
#     if (latDomisili != "undefined") and (lonDomisili != "undefined"):
#         if "first_choice_school" in regis:
#             school = mycolSchool.find_one({"_id": regis["first_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     mycolRegistration.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_domisili_1": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })

#         if "second_choice_school" in regis:
#             school = mycolSchool.find_one({"_id": regis["second_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     mycolRegistration.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_domisili_2": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })
        

# regisArray = mycolRegistration.find({"level_pendaftaran": "perpindahan", "document.perpindahan.perpindahan": {"$exists": True}})

# print("Perpindahan")

# for regis in regisArray:
#     latDomisili = regis["document"]["perpindahan"]["perpindahan"]["coordinate_lat"]
#     lonDomisili = regis["document"]["perpindahan"]["perpindahan"]["coordinate_long"]
#     print(regis["code"])
#     if (latDomisili != "undefined") and (lonDomisili != "undefined"):
#         if "first_choice_school" in regis:
#             school = mycolSchool.find_one({"_id": regis["first_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     mycolRegistration.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_perpindahan_1": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })

#         if "second_choice_school" in regis:
#             school = mycolSchool.find_one({"_id": regis["second_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     mycolRegistration.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_perpindahan_2": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })
        

# # print(calculate(-6.591476,106.783688,-6.5856444,106.7838887))