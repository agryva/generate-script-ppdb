from dbfread import DBF
import json
import pymongo
import os
import datetime
from bson import json_util
import random
import string

def main():
    print("Main")
    #myclient = pymongo.MongoClient("mongodb://ppdbCimahiDev:1@localhost:27017/ppdbCimahiDev")
    #mydb = myclient["ppdbCimahiDev"]
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["ppdb_cimahi"]
    myJunior = mydb["ppdb_unjuniors"]
    myUsers = mydb["ppdb_users"]

    juniors=myJunior.find({})
    for junior in juniors:
        if "school_id" in junior:
            print(junior["_id"])
            myUsers.insert_one({
                "id_unjunior" : junior["_id"],
                "password" : str(randomString()).upper(),
                "school" : junior["school_id"],
                "name" : junior["name"],
                "role" : "elementaryschool-registrant",
                "username" : "2020" + str(junior["un"]).replace("-", ""),
            })


def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))
        
if __name__ == '__main__':
    main()
