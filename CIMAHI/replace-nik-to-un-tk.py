from dbfread import DBF
import json
import pymongo
import os
from bson import json_util

def main():
    print("Main")
    myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
    mydb = myclient["ppdbCimahi"]
    #myclient = pymongo.MongoClient("mongodb://ppdbCimahiDev:1@localhost:27017/ppdbCimahiDev")
    #mydb = myclient["ppdbCimahiDev"]
    #myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    #mydb = myclient["ppdb_cimahi"]
    myJunior = mydb['ppdb_unjuniors']
    myRegis = mydb['ppdb_registrations_2020']

    juniors = myJunior.find({"un_type": "reguler-tk"})

    for junior in juniors:
        myJunior.update_one({"_id": junior["_id"]}, {"$set": {"un": junior["nik"]}})
        myRegis.update_one({"id_junior": junior["_id"]}, {"$set": {"un": junior["nik"]}})
        print(junior["_id"])


if __name__ == '__main__':
    main()
