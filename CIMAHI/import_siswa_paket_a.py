from dbfread import DBF
import json
import os
import pymongo
import datetime
from bson import json_util


def changeMonthNametoNumber(data):
    bulan = str(data).lower()
    if bulan == "januari":
        return 1
    elif bulan == "februari":
        return 2
    elif bulan == "maret":
        return 3
    elif bulan == "april":
        return 4
    elif bulan == "mei":
        return 5
    elif bulan == "juni":
        return 6
    elif bulan == "juli":
        return 7
    elif bulan == "agustus":
        return 8
    elif bulan == "september":
        return 9
    elif bulan == "oktober":
        return 10
    elif bulan == "november":
        return 11
    elif bulan == "desember":
        return 12


myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
mydb = myclient["ppdbCimahi"]
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
myJunior = mydb["ppdb_unjuniors"]
mySchool = mydb["ppdb_schools"]
myUsers = mydb["ppdb_users"]


unjuniors_list = []
dir_data_school = "./PAKET A/BIODATA/"
data_school_folder = os.listdir(dir_data_school)
files = [fname for fname in data_school_folder if fname.endswith('.DBF')]
for file_name in files:
    print(file_name)
    for record in DBF(dir_data_school+file_name):
        tgl = str(record["TGL_LONG"]).split(" ")
        un = record["KD_PROP"] + "-" + record["NOPES"]
        unSplit = str(un).split("-")
        school_paket_a = mySchool.find_one({
            "school_un_smp": unSplit[0]+"-"+unSplit[1]+"-"+unSplit[2],
            "paket_a": True
        })
        unjuniors_list.append({
          "un": un,
          "un_year": "2020",
          "un_type": 'paket a',
          "nisn": record["NISN"],
          "no_induk": record["NO_INDUK"],
          "name": record["NM_PES"],
          "gender": "M" if str(record["SEX"]).lower() == "l" else "F",
          "birth_place": str(record["TMP_LHR"]).upper(),
          "birth_date": datetime.datetime(int(tgl[2]), int(changeMonthNametoNumber(tgl[1])), int(tgl[0]), 0, 0),
        #   "school": record["NM_SEK"],
          "cekun_off": record["CEKUN_OFF"],
          "cekun_on": record["CEKUN_ON"],
          "school_id": school_paket_a["_id"]
        })


for un in unjuniors_list:
    junior = myJunior.insert_one(un)
    myUsers.insert_one({
        "id_unjunior": junior.inserted_id,
        "name": un["name"],
        "username": "1"+str(un["un"]).replace("-", ""),
        "password": "1"+str(un["un"]).replace("-", ""),
        "role" : "elementaryschool-registrant",
        "school": un["school_id"]
    })
    print(junior)