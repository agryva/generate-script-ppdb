from dbfread import DBF
import json
import pymongo
import os
import datetime
from bson import json_util

def main():
    print("Main")
    #myclient = pymongo.MongoClient("mongodb://ppdbCimahiDev:1@localhost:27017/ppdbCimahiDev")
    #mydb = myclient["ppdbCimahiDev"]
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["ppdb_cimahi"]
    myJunior = mydb["ppdb_unjuniors"]
    mySchools = mydb["ppdb_schools"]

    juniors=myJunior.find({})
    for junior in juniors:
        unSplit=str(junior["un"]).split("-")
        un=unSplit[0]+"-"+unSplit[1]+"-"+unSplit[2]
        sc = mySchools.find_one({"school_un": un})
        if sc is not None:
            myJunior.update_one({"_id": junior["_id"]}, {
                "$set": {
                    "school_id": sc["_id"]
                }
            })

if __name__ == '__main__':
    main()
