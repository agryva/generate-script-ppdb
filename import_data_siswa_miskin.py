import pymongo
import xlrd
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
mydb = myclient["ppdb"]
mycolDKTS = mydb["ppdb_dtks"]


def checkValidationData(data):
    if data == "[NULL]" or data == "NULL":
        return "-"
    elif data == "PEREMPUAN":
        return "F"
    elif data == "LAKI-LAKI":
        return "M"
    elif data == "42":
        return ""
    else:
        return data

dir = './DATAMISKIN/'
allfiles = os.listdir(dir)
files = [ fname for fname in allfiles if fname.endswith('.xlsx')]

for fileName in files:
    dirname = os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__)), dir), fileName)      
    wb = xlrd.open_workbook(dirname) 
    sh = wb.sheet_by_index(0)
    dataMiskin=[]
    city=""
    index=0
    for i in range(sh.nrows):
        if i != 0:
            value = sh.row_values(i)
            city=value[2]
            print(value[1].strip()+" "+str(value[6]))
            dataMiskin.append({
                "idbdt": value[1].strip(),
                "city":str(value[2]) if "KOTA" in str(value[2]) else "KAB. "+str(value[2]),
                "district": checkValidationData(str(value[3])),
                "subdistrict": checkValidationData(str(value[4])),
                "name": value[5].strip(),
                "gender": checkValidationData(str(value[6]).strip()),
                "nik": checkValidationData(str(value[7]).strip()),
                "no_kk": checkValidationData(str(value[8]).strip()),
                "umur": int(value[9]),
            })
            i+=1
            print(i)
    # with open('data_miskin_'+city+".json", 'w', encoding='utf-8') as outfile:
    #     json.dump(dataMiskin, outfile, ensure_ascii=False, indent=4)
    mycolDKTS.insert_many(dataMiskin)
    print(city+" Done")

# for fileName in files:
#     dirname = os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__)), dir), fileName)
#     print(dirname)