import pymongo
import xlrd
import datetime
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolSchool = mydb["ppdb_schools"]

arraySchool = mycolSchool.find({
    "address_district": {"$exists": True},
    "address_district": {"$regex": "^KEC. "}
})

for school in arraySchool:
    district = str(school["address_district"]).replace("KEC. ", "")
    
    mycolSchool.update_one({
        "_id": school["_id"]
    }, {
        "$set": {
            "address_district": district
        }
    })
    print(school["_id"])