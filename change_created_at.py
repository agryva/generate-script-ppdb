import pymongo
import json
import os
import datetime
import math

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolRegistration = mydb["ppdb_registrations_2020"]

date = datetime.datetime(2020, 6, 13, 0, 0)
registrations = mycolRegistration.find({
    "createdAt": {
            '$gte': date
        }
})

i = 1
for regis in registrations:
    mycolRegistration.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "createdAt": datetime.datetime(2020, 6, 13, 0, 0)
        }
    })
    print(i)
    i+=1