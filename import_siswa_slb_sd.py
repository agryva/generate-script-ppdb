from dbfread import DBF
import json
import os
import pymongo
import datetime
from bson import json_util

unjuniors_list=[]

def deleteFile(fileName):
    if os.path.isfile(fileName):
        os.remove(fileName)
        print("File Removed!")
    else:
        print ("File not exist")

def changeMonthNametoNumber(data):
    bulan = str(data).lower()
    if bulan == "januari":
        return 1
    elif bulan == "februari":
        return 2
    elif bulan == "maret":
        return 3
    elif bulan == "april":
        return 4
    elif bulan == "mei":
        return 5
    elif bulan == "juni":
        return 6
    elif bulan == "juli":
        return 7
    elif bulan == "agustus":
        return 8
    elif bulan == "september":
        return 9
    elif bulan == "oktober":
        return 10
    elif bulan == "november":
        return 11
    elif bulan == "desember":
        return 12

def search_dbf_file():
    dir_data_school = "./SLB/SD LB JBR/BIO021152020114224/BIOUNSLB/"
    data_school_folder = os.listdir(dir_data_school)
    for folder in data_school_folder:
        dir_un_biodata = os.path.join(dir_data_school, folder + "/" + "BIODATA/")
        alldbf = os.listdir(dir_un_biodata)
        files = [ fname for fname in alldbf if fname.endswith('.DBF')]
        
        for file_name in files:
            open_create_json_file(os.path.join(dir_un_biodata, file_name))

def open_create_json_file(dir_with_filename):
    for record in DBF(dir_with_filename):
        tgl = str(record["TGL_LONG"]).split(" ")
        unjuniors_list.append({
                "type": "open" if record['STS_SEK'] == "N" else "private",
                "level": "elementary",
                "school_un_smp": record['KD_PROP'] + "-" +record['KD_RAYON'] + "-" + record['KD_SEK'],
                "name": record['NM_SEK'],
                "address": record['ALAMAT_1'],
                "address_city": "KOTA CIMAHI",
                "address_province": "JAWA BARAT",
                "address_district": "",
                "address_subdistrict": "",
                "address_rw": "00",
                "address_rt": "00",
                "coordinate_lat": "0.0",
                "coordinate_lng": "0.0",
                "permission": "000",
                "npsn": record['NPSN'],
                "paket_a": True
            })

        print(record["NM_PES"] + "Saved")

def main():
    print("Main")
    
    myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
    # myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["ppdb"]
    myjunior = mydb["ppdb_unjuniors"]

    deleteFile("data_unjuniors_sd_slb.json")
    search_dbf_file()
    
    
    myjunior.insert_many(unjuniors_list)
    # with open('data_unjuniors_sd_slb.json', 'w') as outfile:
    #     json.dump(unjuniors_list, outfile, default=json_util.default)
    # insert_data(cnx)
    # get_data_after_joining(cnx)
    print("Done")


if __name__ == '__main__':
    main()
