import pymongo
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
mydb = myclient["ppdb"]
# mycolJunior = mydb["ppdb_unjuniors"]
mycolSchool = mydb["ppdb_schools"]


dataSchool = mycolSchool.find({"level": "junior"}, no_cursor_timeout=True).batch_size(90)

for school in dataSchool:
    school_un = school['school_un'].split("-")
    school_un_split = school_un[0] + "-"+ school_un[1] + "-"
    if school_un[2][0] == "0":
        school_un_split = school_un_split + (school_un[2][1:len(school_un[2])])
        print(str(school_un) + "  " + str(school_un_split))
        mycolSchool.update_one({'_id': school['_id']}, {
            "$set": {
                "school_un_2017": school_un_split,
            }
        })


# dataJunior = mycolJunior.find({}, no_cursor_timeout=True).batch_size(30)
# i = 0
# for junior in dataJunior:
#     un = junior['un'].split("-")
#     unSplit = "^"+un[0]+"-"+un[1]+"-"+un[2]
#     query = { "school_un": { "$regex": unSplit}}
#     school = mycolSchool.find_one(query)
    
#     if school is not None :
#         mycolJunior.update_one({'_id': junior['_id']}, {
#             "$set": {
#                 "school_id": school['_id'],
#                 "school": school['name']
#             }
#         })
#     i+=1
#     print(i)
