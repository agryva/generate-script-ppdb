import pymongo
import xlrd
import datetime
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolOption = mydb["ppdb_options"]
mycolSchool = mydb["ppdb_schools"]

print("SMA")
seniorSchool = mycolSchool.find({"level": "senior"})
for senior in seniorSchool:
    
    optionSchool = mycolOption.find({
        "school_id": senior["_id"]
    })

    for option in optionSchool:
        if option['type'] == "nhun":
            optionName = str(option['name']).split("-")
            optionNameReal = optionName[0].strip() + " - PRESTASI NILAI RAPOR"
            mycolOption.update_one({
                "_id": option["_id"]
            }, {
                "$set": {
                    "name": optionNameReal
                } 
            })
            print(optionNameReal)

print("\n\n\n")

print("SMK")
seniorSchool = mycolSchool.find({"level": "vocational"})
for senior in seniorSchool:
    
    optionSchool = mycolOption.find({
        "school_id": senior["_id"]
    })

    for option in optionSchool:
        if option['type'] == "nhun":
            optionName = str(option['name']).split("-")
            optionNameReal = optionName[0].strip() + " - " + optionName[1].strip() + " - PRESTASI NILAI RAPOR UMUM"
            mycolOption.update_one({
                "_id": option["_id"]
            }, {
                "$set": {
                    "name": optionNameReal
                } 
            })
            print("NHUN : " + optionNameReal)

        if option['type'] == "nhun-unggulan":
            optionName = str(option['name']).split("-")
            optionNameReal = optionName[0].strip() + " - " + optionName[1].strip() + " - PRESTASI NILAI RAPOR UNGGULAN"
            mycolOption.update_one({
                "_id": option["_id"]
            }, {
                "$set": {
                    "name": optionNameReal
                } 
            })
            print("nhun-unggulan : " + optionNameReal)