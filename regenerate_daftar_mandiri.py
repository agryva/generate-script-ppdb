import pymongo
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolRegistrant = mydb["ppdb_registrations_2020"]

registrant = mycolRegistrant.find({"pernyataan_mutlak": {"$exists": True}})

i=0
for regis in registrant:
    i+=1
    mycolRegistrant.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "cara_pendaftaran": "mandiri"
        }
    })
    print(i)